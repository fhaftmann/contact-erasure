package org.fha.contacterasure

import android.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import java.nio.file.Files.delete
import android.provider.ContactsContract
import android.net.Uri.withAppendedPath
import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun confirmationPrompt(view: View) {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.erase_all_contacts))
            .setMessage(getString(R.string.confirmation_question))
            .setPositiveButton(android.R.string.yes) { _, _ -> erase() }
            .setNegativeButton(android.R.string.no) { _, _ -> }
            .show()
    }

    fun erase() {
        var i = 0
        contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)!!
            .use { cursor ->
                while (cursor.moveToNext()) {
                    try {
                        val key = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY))
                        val uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, key)
                        contentResolver.delete(uri, null, null)
                        i++
                    } catch (e: Exception) {
                        println(e.stackTrace)
                    }
                }
            }
        val notification = Toast.makeText(this, "Erased $i contact(s)", Toast.LENGTH_SHORT)
        notification.show()
    }
}
