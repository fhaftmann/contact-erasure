# contact-erasure

This Android app serves a simple single purpose: on request deleting your local contacts from your android phone.

This is useful if you do not use any kind of synchronization with a web service but provisonize your contacts from a desktop machine.